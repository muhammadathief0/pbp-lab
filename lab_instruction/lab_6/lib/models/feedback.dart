import 'package:flutter/material.dart';

class Feedbacks {
  final String id;
  final String from;
  final Color warna;
  final String message;

  const Feedbacks({
    @required this.id,
    @required this.from,
    this.warna = Colors.orange,
    @required this.message,
  });
}
