from lab_2.models import Note
from django import forms


class noteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'From', 'title', 'message']

        widgets = {
            'to': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Nama Penerima'}),
            'From': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Nama Pengirim'}),
            'title': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Title'}),
            'message': forms.Textarea(attrs={'class': 'input', 'placeholder': 'Message', 'rows': 10, 'cols': 22, 'style': 'resize:none;'})
        }
