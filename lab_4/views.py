from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from .forms import noteForm

# Create your views here.


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    if request.method == 'POST':
        form = noteForm(request.POST or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    else:
        form = noteForm()
        return render(request, 'lab4_form.html', {'form': form})


def note_list(request):
    notes_list = Note.objects.all()
    response = {'notes_list': notes_list}
    return render(request, 'lab4_note_list.html', response)
