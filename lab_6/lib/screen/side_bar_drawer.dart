import 'package:flutter/material.dart';
import 'package:lab_6/screen/navigation_drawer_header.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          NavigationHeader(),
          // UserAccountsDrawerHeader(
          //   accountName: Text("Radit"),
          //   accountEmail: Text("radityahype@compfest.id"),

          // ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {},
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.help,
            title: "About Us",
            onTilePressed: () {},
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.library_books_sharp,
            title: "Features",
            onTilePressed: () {},
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.feedback,
            title: "Feedback",
            onTilePressed: () {},
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.phone,
            title: "Contact Us",
            onTilePressed: () {},
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.login,
            title: "Login",
            onTilePressed: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
