import 'package:flutter/material.dart';

class BuildCard extends StatefulWidget {
  const BuildCard({Key? key}) : super(key: key);

  @override
  _BuildCardState createState() => _BuildCardState();
}

class _BuildCardState extends State<BuildCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Color.fromRGBO(6, 12, 35, 1),
        elevation: 30,
        shadowColor: Color.fromRGBO(23, 194, 236, 1),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
        child: InkWell(
          splashColor: Colors.blue.withAlpha(50),
          borderRadius: BorderRadius.circular(28.0),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            width: 420,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(children: [
                    Text(
                      'Nama: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      'User1',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      'Message: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      "keren guysss <3",
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
