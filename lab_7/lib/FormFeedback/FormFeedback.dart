import 'package:flutter/material.dart';
import 'package:lab_7/Pages/feedbackPage.dart';

class FormFeedback extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _FormFeedbackState();
  }
}

class _FormFeedbackState extends State<FormFeedback> {
  String? _name;
  String? _message;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name', hintText: "Masukkan nama"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Name is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _name = value!;
      },
    );
  }

  Widget _buildMessage() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Message', hintText: "Masukkan pesan"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Message is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _message = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildName(),
            _buildMessage(),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Feedback Berhasil dikirim!')),
                );
                print("Data");
                print("Nama: " + _name!);
                print("Message: " + _message!);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Kirim",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                Navigator.of(context).pushNamed(FeedbackPage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Lihat feedback",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
            // onPressed: () {
            //   if (!_formKey.currentState!.validate()) {
            //     return;
            //   }

            //   _formKey.currentState!.save();

            //   print(_name);
            //   print(_message);

            //   //Send to API
            // },
          ],
        ),
      ),
    );
  }
}
