import 'package:flutter/material.dart';
import 'package:lab_7/Pages/feedbackPage.dart';
import 'package:lab_7/Pages/homePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid Assistant',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "Poppins",
      ),
      home: MyHomePage(),
      initialRoute: MyHomePage.routeName,
      routes: {
        MyHomePage.routeName: (context) => MyHomePage(),
        FeedbackPage.routeName: (context) => FeedbackPage(),
      },
    );
  }
}

// class MyHomePage extends StatelessWidget {
//   int index = 2;
//   final items = <Widget>[
//     Icon(
//       Icons.home,
//       size: 30,
//     ),
//     Icon(
//       Icons.search,
//       size: 30,
//     ),
//     Icon(
//       Icons.favorite,
//       size: 30,
//     ),
//     Icon(
//       Icons.settings,
//       size: 30,
//     ),
//     Icon(
//       Icons.person,
//       size: 30,
//     ),
//   ];
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       extendBody: true,
//       drawer: DrawerScreen(),
//       body: Container(
//         decoration: BoxDecoration(
//           color: Color.fromRGBO(6, 12, 35, 1),
//         ),
//         child: SingleChildScrollView(
//           child: Column(
//             children: <Widget>[
//               Navbar(),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                     vertical: 20.0, horizontal: 40.0),
//                 child: LandingPage(),
//               ),
//               Container(
//                 decoration: BoxDecoration(
//                   color: Color.fromRGBO(232, 247, 252, 1),
//                 ),
//                 child: Column(
//                   children: <Widget>[
//                     SizedBox(
//                       height: 100,
//                     ),
//                     Text(
//                       "ABOUT US",
//                       style: TextStyle(
//                         color: Color.fromRGBO(23, 194, 236, 1),
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold,
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.symmetric(
//                           vertical: 20.0, horizontal: 40.0),
//                       child: BuildCard(
//                         from: 'User1',
//                         message: 'keren guysss <3',
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.symmetric(
//                           vertical: 4.0, horizontal: 40.0),
//                       child: BuildCard(
//                         from: 'User2',
//                         message: 'mantapp guysss <3',
//                       ),
//                     ),
//                     SizedBox(
//                       height: 100,
//                     ),
//                     Text(
//                       "FEEDBACK",
//                       style: TextStyle(
//                         color: Color.fromRGBO(23, 194, 236, 1),
//                         fontSize: 20,
//                         fontWeight: FontWeight.w900,
//                         fontFamily: "Poppins",
//                       ),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Text(
//                       "Tulis Feedback Anda Tentang Kami",
//                       textAlign: TextAlign.center,
//                       style: TextStyle(
//                         color: Colors.black,
//                         fontSize: 26,
//                         fontWeight: FontWeight.bold,
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.symmetric(
//                           vertical: 50.0, horizontal: 40.0),
//                       child: FormFeedback(),
//                     ),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//       bottomNavigationBar: CurvedNavigationBar(
//         color: Color.fromRGBO(23, 194, 236, 1),
//         backgroundColor: Colors.transparent,
//         items: items,
//         index: index,
//         height: 50,
//       ),
//     );
//   }
// }
