import 'package:flutter/material.dart';
import 'package:lab_7/Pages/feedbackPage.dart';
import 'package:lab_7/Pages/homePage.dart';
import 'package:lab_7/screen/navigation_drawer_header.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          NavigationHeader(),
          // UserAccountsDrawerHeader(
          //   accountName: Text("Radit"),
          //   accountEmail: Text("radityahype@compfest.id"),

          // ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onClicked: () => selectedItem(context, 0),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.help,
            title: "About Us",
            onClicked: () => selectedItem(context, 1),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.library_books_sharp,
            title: "Features",
            onClicked: () => selectedItem(context, 2),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.feedback,
            title: "Feedback",
            onClicked: () => selectedItem(context, 3),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.phone,
            title: "Contact Us",
            onClicked: () => selectedItem(context, 4),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.login,
            title: "Login",
            onClicked: () => selectedItem(context, 5),
          ),
        ],
      ),
    );
  }
}

void selectedItem(BuildContext context, int index) {
  Navigator.of(context).pop();

  switch (index) {
    case 0:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MyHomePage(),
      ));
      break;
    case 3:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => FeedbackPage(),
      ));
      break;
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onClicked;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onClicked})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onClicked,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
