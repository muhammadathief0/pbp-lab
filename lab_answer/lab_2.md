1. Apakah perbedaan antara JSON dan XML?
Jawab:
Perbedaan JSON dan XML sebagai berikut:
- Berdasarkan pengertiannya keduanya memiliki perbedaan yaitu JSON adalah
turunan dari object JavaScript. Sedangkan XML adalah singkatan dari eXtensible Markup Language.
- JSON adalah bahasa meta, sedangkan XML adalah  bahasa markup
- Dari segi kompleksitasnya JSON lebih sederhana dan mudah dibaca, sedangkan XML Lebih rumit
- Dari segi orientasinya JSON berorientasi pada data, sedangkan XML beroirentasi pada dokumen.
- JSON mendukung penggunaaan array, sedangkan XML tidak mendukung penggunaaan array.
- File JSON diakhiri dengan ekstensi .json, sedangkan file XML diakhiri dengan ekstensi .xml.


2. Apakah perbedaan antara HTML dan XML?
Jawab: 
- Berdasarkan pengertiannya HTML adalah singkatan dari Hypertext Markup Language, sedangkan XML adalah singkatan dari eXtensible Markup Language. Ini menunjukkan bahwa keduanya jelas berbeda.
- Berdasarkan strukturalnya HTML tidak mengandung informasi struktural, sedangkan XML informasinya disediakan.
- Berdasarkan tujuannya HTML memiliki tujuan untuk penyajian data, sedangkan XML untuk transfer data.
- HTML itu Case Insensitive sedangkan XML itu Case Sensitive
- Pada HTML error kecil dapat diabaikan, sedangkan pada XML tidak boleh ada error.
- Pada HTML tag penutup bisa ada atau tidak (opsional), sedangkan XML harus menggunakan tag penutup.
- Untuk urutannya pada HTML tidak terlalu diperhitungkan, sedangkan pada XML urutannya harus dilakukan dengan benar.


Referensi:
- https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
- https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html